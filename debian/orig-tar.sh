#!/bin/sh -e

VERSION=$2
TAR=../plexus-digest_$VERSION.orig.tar.gz
DIR=plexus-digest-$VERSION
TAG=plexus-digest-$VERSION

svn export http://svn.codehaus.org/plexus/plexus-components/tags/$TAG/ $DIR

# the original jar ships a class file without source
(cd $DIR/src/test/examples && \
 echo "fake file" > redback-authz-open.jar && \
 md5sum redback-authz-open.jar > redback-authz-open.jar.md5 && \
 sha1sum redback-authz-open.jar > redback-authz-open.jar.sha1)

tar -c -z -f $TAR $DIR
rm -rf $DIR ../$TAG
